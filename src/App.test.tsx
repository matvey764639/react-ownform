import { getByText, render, screen } from "@testing-library/react";
import App from "./App";

describe("App tests", () => {
    it("app renders", () => {
        render(<App />);
        expect(screen.getByText(/hello/i)).toBeInTheDocument();
    });

});
/*


* */
