import React from "react";

interface MyFormGroupProps {
    children?: any;
}

const MyFormGroup: React.FC<MyFormGroupProps> = ({ children }) => {
    return <div className="mt-3">{children}</div>;
};

export default React.memo(MyFormGroup);
