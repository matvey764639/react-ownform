import React from "react";

interface MyFormLabelProps {
    children?: any;
}

const MyFormLabel: React.FC<MyFormLabelProps> = (props) => {
    return <div>{props.children}</div>;
};

export default React.memo(MyFormLabel);
