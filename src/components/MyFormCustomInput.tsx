import React, { useState } from "react";

interface MyFormCustomInputProps {
    type: string;
    value?: string;
    onChange?: (value: string) => void;
    placeholder?: string;
}

const MyFormCustomInput: React.FC<MyFormCustomInputProps> = ({
    type,
    value,
    onChange,
    placeholder,
}) => {
    //const [myValue, setMyValue] = useState<string>();
    return value === undefined || onChange === undefined ? (
        <input
            type={type}
            placeholder={placeholder}
            className="border rounded-2 w-100"
        />
    ) : (
        <input
            type={type}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                onChange(e.target.value)
            }
            className="border rounded-2 w-100"
        />
    );
};

export default React.memo(MyFormCustomInput);
