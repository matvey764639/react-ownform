import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import MyFormLabel from "./MyFormLabel";
import MyFormGroup from "./MyFormGroup";
import MyFormCheckbox from "./MyFormCheckbox";
import MyFormCustomInput from "./MyFormCustomInput";
import MyForm from "./MyForm";
import Select from "react-select";


interface ListItem{
    value:string;
    label:string;
}
interface MyFormTestProps {
    listValues1: string[];
    listValues2: ListItem[];
}

const MyFormTest: React.FC<MyFormTestProps> = (props) => {
    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {

        event.preventDefault();
        console.clear();
        console.log(event.currentTarget.elements.length);
        console.log(event.currentTarget.elements);
        for (let i = 0; i < event.currentTarget.length - 2; i++) {
            if (event.currentTarget.elements[i].value === "on") {
                console.log(event.currentTarget.elements[i].checked);
            } else {
                console.log(event.currentTarget.elements[i].value);
            }
        }

        //event.currentTarget.reset();
        /*
        event.preventDefault();
        console.clear();
        console.log(event.currentTarget.elements[0].value);
        console.log(event.currentTarget.elements[1].checked);
        console.log(event.currentTarget.elements[2].checked);
        console.log(event.currentTarget.elements[3].checked);
        console.log(event.currentTarget.elements[5].value);
        console.log(event.currentTarget.elements[7].value);
        console.log(event.currentTarget.elements[8].value);
        console.log(event.currentTarget.elements[9].value);
        console.log(event.currentTarget.elements[10].value);
        console.log(event.currentTarget.elements[11].value);
        //event.currentTarget.reset();
        */
    };

    return (
        <MyForm
            className="position-absolute start-50 translate-middle-x mt-5 bg-light"
            handleSubmit={handleSubmit}
        >
            <MyFormGroup>
                <MyFormLabel>First input</MyFormLabel>
                <MyFormCustomInput type="text" placeholder="Введите строку"/>
            </MyFormGroup>
            <MyFormGroup>
                <MyFormLabel>Second input</MyFormLabel>
                <MyFormCheckbox label="first"/>
                <MyFormCheckbox label="second"/>
                <MyFormCheckbox label="third"/>
            </MyFormGroup>
            <MyFormGroup>
                <MyFormLabel>Third input</MyFormLabel>
                <Select options={props.listValues2} className="w-100"/>
            </MyFormGroup>
            <MyFormGroup>
                <MyFormLabel>Fourth input</MyFormLabel>
                <Select
                    options={props.listValues2}
                    isMulti={true}
                    className="w-100"
                />
            </MyFormGroup>
            <MyFormGroup>
                <MyFormLabel>Fifth input</MyFormLabel>
                <MyFormCustomInput type="file"/>
            </MyFormGroup>
            <MyFormGroup>
                <MyFormLabel>Sixth input</MyFormLabel>
                <MyFormCustomInput type="date"/>
            </MyFormGroup>
            <MyFormGroup>
                <MyFormLabel>Seventh input</MyFormLabel>
                <MyFormCustomInput type="password"/>
            </MyFormGroup>
            <MyFormGroup>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
                <Button variant="danger">Cancel</Button>
            </MyFormGroup>
        </MyForm>
    );
};

export default React.memo(MyFormTest);
