import React from "react";
interface MyFormCheckboxProps {
    label?: string;
}

const MyFormCheckbox: React.FC<MyFormCheckboxProps> = ({ label}) => {
    return (
        <div>
            <input type="checkbox" className="d-inline" />
            <div className="d-inline">{label}</div>
        </div>
    );
};

export default React.memo(MyFormCheckbox);
