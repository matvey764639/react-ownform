import React from "react";
interface MyFormProps {
    children?: any;
    className?: string;
    handleSubmit: (event:  React.FormEvent<HTMLFormElement>) => void;
}

const MyForm: React.FC<MyFormProps> = ({
    children,
    className,
    handleSubmit,
}) => {
    return (
        <form
            className={className + " border border-2 rounded-3"}
            onSubmit={handleSubmit}
        >
            {children}
        </form>
    );
};

export default React.memo(MyForm);
