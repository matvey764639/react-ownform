import React from "react";
import Select from "react-select";
interface MyFormSelectListProps {
    value: { value: string; label: string }[];
    size?: number;
}

const MyFormSelectList: React.FC<MyFormSelectListProps> = ({ value, size }) => {
    return <Select options={value} className="w-100" />;
};

export default React.memo(MyFormSelectList);
