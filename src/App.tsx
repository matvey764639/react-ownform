import style from "./App.module.less";
import { useState } from "react";
import MyFormTest from "./components/MyFormTest";

function App() {
    const listValues: string[] = [
        "first list item",
        "second list item",
        "third list item",
        "fourth list item",
        "fifth list item",
    ];
    const listValues2 = [
        { value: "first list item", label: "First list item" },
        { value: "second list item", label: "Second list item" },
        { value: "third list item", label: "Third list item" },
        { value: "fourth list item", label: "Fourth list item" },
        { value: "fifth list item", label: "Fifth list item" },
    ];
    return (
        <MyFormTest
            listValues1={listValues}
            listValues2={listValues2}
        ></MyFormTest>
    );
}

export default App;
